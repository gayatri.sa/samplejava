FROM tomcat:latest

RUN apt-get update
COPY ./target/junitwebapp.war /usr/local/tomcat/webapps/junitwebapp.war
